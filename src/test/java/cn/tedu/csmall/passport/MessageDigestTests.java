package cn.tedu.csmall.passport; 
 /*
 @Time: 2022/9/7 14:19
 @Author:GShuai
 @File:MessageDigestTests.class
 @Software:IntelliJ IDEA
*/


import org.junit.jupiter.api.Test;
import org.springframework.util.DigestUtils;

import java.util.UUID;

public class MessageDigestTests {

    @Test
    public void testMd5(){
        for (int i = 0; i < 5; i++) {
            String salt = UUID.randomUUID().toString();
            String rawPassword = "123456";
            String encodedPassword = DigestUtils.md5DigestAsHex((salt + rawPassword).getBytes());
            System.out.println("rawPassword = " + rawPassword);
            System.out.println("salt = " + salt);
            System.out.println("encodedPassword = " + encodedPassword);
            System.out.println();
        }
    }


    @Test
    public void testUUID(){
        for (int i = 0; i <20 ; i++) {
            System.out.println(UUID.randomUUID());
        }
    }
}
