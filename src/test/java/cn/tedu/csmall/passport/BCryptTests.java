package cn.tedu.csmall.passport; 
 /*
 @Time: 2022/9/7 23:34
 @Author:GShuai
 @File:BCryptTests.class
 @Software:IntelliJ IDEA
*/


import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class BCryptTests {

    @Test
    public void testBCrypt(){
        BCryptPasswordEncoder PasswordEncoder = new BCryptPasswordEncoder();
        String rawPassword = "123456";
        for (int i = 0; i <10 ; i++) {
            String encodePassword = PasswordEncoder.encode(rawPassword);
            System.out.println("原密码："+rawPassword+",密文："+encodePassword);
            //    $2a$10$R2is4KBPvxtV/zL1LCj22eloJDfVZuQwV7DSNISnB2EGr7j8knJoW
        }
    }

    @Test
    public void testMatch(){
        String rawPassword= "123456";
        String encodePassword ="$2a$10$R2is4KBPvxtV/zL1LCj22eloJDfVZuQwV7DSNISnB2EGr7j8knJoW";
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        boolean matches = passwordEncoder.matches(rawPassword,encodePassword);
        System.out.println("原密码："+rawPassword+",密文："+encodePassword);
        System.out.println("匹配结果："+matches);
    }
}
