package cn.tedu.csmall.passport; 
 /*
 @Time: 2022/9/8 22:40
 @Author:GShuai
 @File:JWTTests.class
 @Software:IntelliJ IDEA
*/


import io.jsonwebtoken.*;
import org.junit.jupiter.api.Test;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JWTTests {
    // 密钥
    String secretKey = "adfajfgionwfgwkcd%vjnwpov&scnvcpgoqwbngfdfdsfdsfs";

    @Test
    public void testGenerateJwt() {
        // 准备Claims
        Map<String, Object> claims = new HashMap<>();
        claims.put("id", 95130);
        claims.put("name", "张三");

        // 准备过期时间：1分钟
        Date expirationDate = new Date(System.currentTimeMillis() + 10 * 60 * 1000);


        // JWT的组成部分：Header（头）、Payload（载荷）、Signature（签名）
        String jwt = Jwts.builder()
                // Header：用于配置算法与此结果数据的类型
                // 通常配置2个属性：typ（类型）、alg（算法）
                .setHeaderParam("typ", "jwt")
                .setHeaderParam("alg", "HS256")
                // Payload：用于配置需要封装到JWT中的数据
                .setClaims(claims)
                .setExpiration(expirationDate)
                // Signature：用于指定算法与密钥（盐）
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
        System.out.println(jwt);

        // eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9
        // .
        // eyJuYW1lIjoi5byg5LiJIiwiaWQiOjk1MTMwLCJleHAiOjE2NjI2NTEwMDJ9
        // .
        // RSVw-aKlFgj8GPUvtli15IxRLgan8t2sUqQEM1k4FDI
    }

    @Test
    public void testParseJwt(){
        String jwt="eyJ0eXBlIjoiand0IiwiYWxnIjoiSFMyNTYifQ.eyJleHAiOjE2NjMzMTY0NTcsInVzZXJuYW1lIjoi5p2O6ICB5biIIn0.IRBKSwx0fvlnm5tl_RHWZIiL7WS-rp4qDK-lzKeH7Ho";
        Claims claims = Jwts.parser().setSigningKey("sdfgdsfgwgsuisdnvs").parseClaimsJws(jwt).getBody();
        Object username = claims.get("username");
        System.out.println("username="+username);
//        Object id = claims.get("id");
//        Object name = claims.get("name");
//        System.out.println("id = " +id);
//        System.out.println("name = "+name);
    }

}
