package cn.tedu.csmall.passport.security;

import lombok.Data;

import java.io.Serializable;

/*
 @Time: 2022/10/4 23:17
 @Author:GShuai
 @File:LoginPrincipal.class
 @Software:IntelliJ IDEA
*/

/**
 * 当前登录的当事人
 *
 */
@Data
public class LoginPrincipal implements Serializable {
    /**
     * 当前登录用户的id
     */

    private Long id;
    /**
     * 当前登录用户的用户名
     */
    private String username;
}
