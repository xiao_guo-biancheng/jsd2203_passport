package cn.tedu.csmall.passport.mapper; 
 /*
 @Time: 2022/9/6 23:17
 @Author:GShuai
 @File:AdminMapper.class
 @Software:IntelliJ IDEA
*/


import cn.tedu.csmall.passport.pojo.entity.Admin;
import cn.tedu.csmall.passport.pojo.vo.AdminListItemVO;
import cn.tedu.csmall.passport.pojo.vo.AdminLoginVO;
import cn.tedu.csmall.passport.pojo.vo.AdminSimpleVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 管理员Mapper接口
 *
 * @version 0.0.1
 */
@Repository
public interface AdminMapper {
    /**
     * 插入管理员数据
     *
     * @param admin 管理员数据
     * @return 受影响的行数，成功插入数据时，将返回1
     */
    int insert(Admin admin);

    /**
     * 根据管理员id删除管理员
     *
     * @param id 管理员id
     * @return 受影响的行数
     */
    int deleteById(Long id);

    /**
     * 根据管理员用户名统计此用户名对应的管理员数据的数量
     *
     * @param username 管理员用户名
     * @return 此名称对应的管理员数据的数量
     */
    int countByUsername(String username);

    /**
     * 根据id获取管理员的简单信息
     *
     * @param id 管理员id
     * @return 匹配的管理员的简单信息，如果没有匹配的管理员信息，则返回null
     */
    AdminSimpleVO getSimpleById(Long id);

    /**
     * 根据管理员用户名查询管理员的登录相关信息
     *
     * @param username 管理员用户名
     * @return 匹配的管理员相关信息，如果没有匹配的数据，则返回null
     */
    AdminLoginVO getByUsername(String username);

    /**
     * 查询管理员列表
     *
     * @return 管理员列表的集合
     */
    List<AdminListItemVO> list();
}
