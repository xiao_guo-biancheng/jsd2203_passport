package cn.tedu.csmall.passport.mapper; 
 /*
 @Time: 2022/10/12 19:14
 @Author:GShuai
 @File:AdminRoleMapper.class
 @Software:IntelliJ IDEA
*/

import cn.tedu.csmall.passport.pojo.entity.AdminRole;
import org.springframework.stereotype.Repository;

/**
 * 管理员与角色的关联关系的持久层Mapper接口
 *
 * @version 0.0.1
 */
@Repository
public interface AdminRoleMapper {
    /**
     * 插入管理员与角色的关联数据
     *
     * @param adminRole 管理员与角色的关联数据
     * @return 受影响的行数
     */
    int insert(AdminRole adminRole);
}
