package cn.tedu.csmall.passport.config;
 /*
 @Time: 2022/8/30 10:12
 @Author:GShuai
 @File:MybatisConfiguration.class
 @Software:IntelliJ IDEA
*/

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;


@Slf4j
@Configuration
@MapperScan("package cn.tedu.csmall.passport.mapper")
public class MybatisConfiguration {
    public MybatisConfiguration() {
        log.debug("加载配置类：MybatisConfiguration");
    }
}
