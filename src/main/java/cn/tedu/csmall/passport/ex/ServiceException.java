package cn.tedu.csmall.passport.ex;
 /*
 @Time: 2022/9/2 18:36
 @Author:GShuai
 @File:ServiceException.class
 @Software:IntelliJ IDEA
*/


public class ServiceException extends RuntimeException {
    private Integer serviceCode;

    public ServiceException(Integer serviceCode,String message) {
        super(message);
        this.serviceCode = serviceCode;
    }
    public Integer getServiceCode(){
        return serviceCode;
    }

}
