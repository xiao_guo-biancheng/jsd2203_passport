package cn.tedu.csmall.passport.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 管理员简单信息（此类中的属性数量并不重要，均暂时不会使用到，主要用于检验管理员数据是否存在）
 */
@Data
public class AdminSimpleVO implements Serializable {

    /**
     * 数据id
     */
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 昵称
     */
    private String nickname;

}
