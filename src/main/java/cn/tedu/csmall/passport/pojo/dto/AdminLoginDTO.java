package cn.tedu.csmall.passport.pojo.dto; 
 /*
 @Time: 2022/9/7 8:22
 @Author:GShuai
 @File:AdmnLoginDTO.class
 @Software:IntelliJ IDEA
*/

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 管理员登录数据
 *
 * @version 0.0.1
 */
@Data
public class AdminLoginDTO implements Serializable {

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String username;

    /**
     * 密码（密文）
     */
    @ApiModelProperty(value = "密码")
    private String password;
}