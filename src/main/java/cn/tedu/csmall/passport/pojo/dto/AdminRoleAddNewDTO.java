package cn.tedu.csmall.passport.pojo.dto; 
 /*
 @Time: 2022/10/12 19:14
 @Author:GShuai
 @File:AdminRoleAddNewDTO.class
 @Software:IntelliJ IDEA
*/

import java.io.Serializable;

/**
 * 新增管理员与角色的关联数据的DTO类
 * @version 0.0.1
 */

public class AdminRoleAddNewDTO implements Serializable {
    /**
     * 管理员id
     */
    private Long adminId;

    /**
     * 角色id
     */
    private Long roleId;

}
