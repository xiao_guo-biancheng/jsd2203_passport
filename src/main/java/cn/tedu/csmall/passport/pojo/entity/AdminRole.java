package cn.tedu.csmall.passport.pojo.entity; 
 /*
 @Time: 2022/10/12 19:15
 @Author:GShuai
 @File:AdminRole.class
 @Software:IntelliJ IDEA
*/

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 管理员与角色的关联数据的实体类
 *
 * @version 0.0.1
 */
@Data
public class AdminRole implements Serializable {
    /**
     * 数据id
     */
    private Long id;

    /**
     * 管理员id
     */
    private Long adminId;

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 数据创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 数据最后修改时间
     */
    private LocalDateTime gmtModified;
}
